Vim syntax highlighting file for Cython with partial Pyrex compatibility

---


Introduction
------------

This syntax file is a fork based on the `syntax/pyrex.vim` distributed with Vim
as of version 8.1.

The syntax has been improved for compatibility with Cython. Certain
backward-compatible grammars (such as the `for ... from ... by ...` loop) are
also supported.


Features
--------

- Almost full Cython syntax support, including:
    - grammar features for C++ interoperability (such as the `new` operator,
  `namespace` in external declarations, and `cppclass`),
    - the `packed` attribute for `cdef`ed `struct`s,
    - `U`, `UL`, `LL`, and `ULL` integer-literal suffixes,
    - built-in types, such as `bint`,
    - built-in attributes, such as `fused`,
    - compile-time directives,
    - directives to `cythonize` in header comment block, etc.
- Pyrex-compatible syntax, such as `by` as a keyword in the loop construct.
- Greater simplicity and seemingly faster performance than
  [`vim-cython`](https://github.com/anntzer/vim-cython).


Notes
----

The syntax script sources the Python syntax file. It works the best with my
modified simple Python syntax file [`vim-syntax-python`](https://github.com/congma/vim-syntax-python).
