" Vim syntax file
" Language:	Pyrex
" Maintainer:	Cong Ma <m.cong@protonmail.ch>
" URL:		https://gitlab.com/congma/vim-pyrex-cython
" Last Change:	2019 Aug 27

" quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif
let s:cpo_save = &cpo
set cpo&vim

" Cython compile-time directives
syn keyword pyrexDirective	DEF IF ELIF ELSE


" Cython context-specific keywords
syn match   pyrexExtDecl	+\_^\_s*cdef\s\+\%(extern\|import\)\s\+from\s\+["'\.\f]\+.*:+ transparent fold
syn keyword pyrexStatementAdd	namespace contained containedin=pyrexExtDecl
syn match   pyrexOldFor		"\_^\_s*for\s\+\i\+\s\+from\s\+.*:" transparent
syn keyword pyrexStatementAdd	from by contained containedin=pyrexOldFor
syn match   pyrexCdefStruct	"\_^\_s*cdef\s\+\%(packed\)\?\s\+struct\s\+\i\+" transparent
syn keyword pyrexTypePacked	packed contained containedin=pyrexCdefStruct

" Cython C-style integer literals with suffixes
syn match   pyrexIntegerLiteral	"\<0[oO]\%(_\=\o\)\+[Uu]\?[Ll]\{,2}\>"
syn match   pyrexIntegerLiteral	"\<0[xX]\%(_\=\x\)\+[Uu]\?[Ll]\{,2}\>"
syn match   pyrexIntegerLiteral	"\<0[bB]\%(_\=[01]\)\+[Uu]\?[Ll]\{,2}\>"
syn match   pyrexIntegerLiteral	"\<\%([1-9]\%(_\=\d\)*\|0\+\%(_\=0\)*\)[Uu]\?[Ll]\{,2}\>"

" Read the Python syntax to start with
runtime! syntax/python.vim
unlet b:current_syntax

" Cython language directive
if !exists("python_no_file_header")
  syn match pyrexDist "\%^.*\%(\n.*\)\?#\s*\<distutils:\s*\<\%(sources\|language\)\>\s*=.*$"
  syn match pyrexDist "\%^.*\%(\n.*\)\?#\s*cython:\s*\k\+\%(\.\k+\)*\s*=.*$"
endif

" Pyrex extentions
syn keyword pyrexStatement      ctypedef sizeof new typeid
syn match   pyrexCtypedef	"\_^\_s*ctypedef\s\+\i\+" transparent
syn match   pyrexFunction
      \ "\%(\_^\_s*\%(cdef\|cpdef\)\s\+\%(\k\+\s\+\)*\s*\)\@128<=\%(\k\+\)\ze\s*(.*)\%(\s*\S*\s*\)*:" fold
syn keyword pyrexStatement	cpdef cdef nextgroup=pyrexFunction,pyrexType,pyrexAccess skipwhite
syn keyword pyrexStatement	cppclass nextgroup=pythonFunction,pyrexFunction,pyrexAccess skipwhite
syn keyword pyrexType		int long short float double char object void
syn keyword pyrexType		signed unsigned
syn keyword pyrexType		Py_ssize_t Py_hash_t Py_uhash_t
syn keyword pyrexType		Py_UNICODE Py_UCS4
syn keyword pyrexType		const complex size_t bint ssize_t ptrdiff_t
syn keyword pyrexType		list tuple dict
syn keyword pyrexTypeFused	fused contained containedin=pyrexCtypedef
syn keyword pyrexStructure	struct union enum
syn keyword pyrexInclude	include cimport
syn keyword pyrexAccess		public private property readonly extern inline
syn keyword pyrexAccess		nogil gil
" If someome wants Python's built-ins highlighted probably he
" also wants Pyrex's built-ins highlighted
if (!exists("python_no_builtin_highlight")) ||
   \ exists("pyrex_highlight_builtins")
  syn keyword pyrexBuiltin	NULL
  syn keyword pyrexBuiltin	UNAME_SYSNAME UNAME_NODENAME UNAME_RELEASE
  syn keyword pyrexBuiltin	UNAME_VERSION UNAME_MACHINE
  " add the following names back to the pythonBuiltin space (obsolete in Py3k)
  syn keyword pythonBuiltin	intern reload
endif

" Default highlighting
hi def link pyrexIntegerLiteral	Number
hi def link pyrexStatement	Statement
hi def link pyrexFunction	Function
hi def link pyrexType		Type
hi def link pyrexInclude	PreCondit
hi def link pyrexDirective	PreProc
hi def link pyrexStructure	Structure
hi def link pyrexStatementAdd	pyrexStatement
hi def link pyrexAccess		pyrexStatement
hi def link pyrexTypeFused	pyrexType
hi def link pyrexTypePacked	pyrexType
if (!exists("python_no_builtin_highlight")) ||
   \ exists("pyrex_highlight_builtins")
  hi def link pyrexBuiltin	Constant
endif
if !exists("python_no_file_header")
  hi def link pyrexDist		Special
endif

let b:current_syntax = "pyrex"

let &cpo = s:cpo_save
unlet s:cpo_save
" vim:set sw=2 sts=2 ts=8 noet:
